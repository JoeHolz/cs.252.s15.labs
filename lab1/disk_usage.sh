#! /bin/bash
if [ $# -gt 1 ];then
du $2 2> /dev/null | sort -h | tail  --lines=$1
else
du $1 2> /dev/null | sort -h | tail --lines=10
fi
