#! /bin/bash
case $1 in
sum)
	let sum=0
	while read line
		do
		sum=$((sum + line))
	done <input
	echo $sum
	;;
mean)
	let count=0
	let sum=0
	while read line 
		do
		sum=$((sum + line))
		count=$((count + 1))
	done <input
	echo $((sum / count))
	;;
min)
	let min=$((1000000000))
	while read line
		do
		i=$line
		if [ $min -gt $i ];then
		min=$i
		fi
	done <input
	echo $min
	;;
max)
	let max=$((0))
	while read line
	do
		i=$line
		if [ $max -lt $i ];then
		max=$i
		fi
	done <input
	echo $max
	;;
*)
	echo "usage: ./statistics.sh [sum | mean | min | max]"
	;;
esac
