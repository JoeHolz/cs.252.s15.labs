#! /bin/bash
if [ $# == 1 ]; then
	a=$(ps | grep $1 | cut -c1-5)
	kill $a
else
	echo "usage: ./process_killer.sh program"
fi
