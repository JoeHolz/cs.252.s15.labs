#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NSETS   1024

void bitset_add(uint64_t sets[], int set, int value)
{
    sets[set] = sets[set] | (1UL<<value);
}
void bitset_get(uint64_t sets[], int set, int value)
{
    if (sets[set] & (1UL<<value)) {
        puts("Yes");
    } else {
        puts("No");
    }
}
void bitset_list(uint64_t sets[], int set){
	for(unsigned long int i =0; i < sizeof(sets[set])*8;i++){
		if(sets[set] & (1UL<<i)){
			printf("%lu\n",i);
		}		
	}
}
void bitset_delete(uint64_t sets[],int set,int value){
	sets[set] = sets[set] & ~(1UL<<value); 
}
void bitset_binary(uint64_t sets[],int set){
	char thing [sizeof sets[set]*8 +1];
	for(int i =0; i < sizeof(sets[set])*8+1;i++){
		if(sets[set] & (1UL<<i)){
			thing[sizeof(sets[set])*8 - i - 1]='1';
		}else{
			thing[sizeof(sets[set])*8 - i - 1]='0';
		}
	}
		thing[64]=0;
		printf("%s\n",thing);
}
void bitset_hex(uint64_t sets[],int set){
	printf("%x\n",(unsigned int)sets[set]);
}
void bitset_dec(uint64_t sets[],int set){
	printf("%d\n",(int)sets[set]);
}
void bitset_count(uint64_t sets[],int set){
	int count = 0;	
	int a = sets[set];
	while(a){
		count++;
		a&=a-1;
	}
	printf("%d\n",count);
}
void bitset_max(uint64_t sets[],int set){
	for(int i = sizeof(sets[set])*8;i > 0; i--){
		if(sets[set] & 1UL<<i){
			printf("%d\n",i);
			return;			
		}
	}
	printf("MAX not found, empty set.\n");	
}
void bitset_min(uint64_t sets[],int set){
	for(int i = 0; i < sizeof(sets[set])*8; i++){
		if(sets[set] & 1UL<<i){
			printf("%d\n",i);
			return;
		}
	}
	printf("MIN not found, empty set.\n");
}
void bitset_union(uint64_t sets[],int set, int value,int dest){
	sets[dest] = sets[set] | sets[value];
}
void bitset_diff(uint64_t sets[],int set, int value,int dest){
	sets[dest] = sets[set] ^ sets[value];
}
void bitset_dist(uint64_t sets[],int set,int value){
	int dist = 0;
	unsigned long int val = sets[set] ^ sets[value];
	while(val){
		dist++;
		val &= val-1;
	}
	printf("%d",dist);
}
int main(int argc, char *argv[])
{
    char     buffer[BUFSIZ];
    uint64_t command = 0;
    uint64_t sets[NSETS] = {0};
    
    while (true) {
        int set;
        int value;
	int dest;
        printf("[%4lu> ", command);
        fflush(stdout);

        if (fgets(buffer, BUFSIZ, stdin) == NULL) {
            break;
        }

        if (sscanf(buffer, "ADD %d %d", &set, &value) == 2) {
            bitset_add(sets, set, value);
        } else if (sscanf(buffer, "GET %d %d", &set, &value) == 2) {
            bitset_get(sets, set, value);
        } else if(sscanf(buffer,"LIST %d",&set) == 1){
	    bitset_list(sets,set);
	} else if(sscanf(buffer,"DEL %d %d",&set,&value) == 2){
	    bitset_delete(sets,set,value);
	} else if(sscanf(buffer,"BIN %d",&set) == 1){
		bitset_binary(sets,set);
	} else if(sscanf(buffer,"HEX %d",&set) == 1){
		bitset_hex(sets,set);
	} else if(sscanf(buffer,"DEC %d",&set) == 1){
		bitset_dec(sets,set);
	} else if(sscanf(buffer,"COUNT %d",&set) == 1){
		bitset_count(sets,set);
	} else if(sscanf(buffer,"MAX %d",&set) == 1){
		bitset_max(sets,set);
	} else if(sscanf(buffer,"MIN %d",&set) == 1){
		bitset_min(sets,set);
	} else if(sscanf(buffer,"UNION %d %d %d",&set,&value,&dest) == 3){
		bitset_union(sets,set,value,dest);
	} else if(sscanf(buffer,"DIFFERENCE %d %d %d",&set,&value,&dest) == 3){
		bitset_diff(sets,set,value,dest);
	} else if(sscanf(buffer,"DISTANCE %d %d",&set,&value) == 2){
		bitset_dist(sets,set,value);
	}
	else {
	    printf("Unknown command: %s", buffer);
        }
        command++;
    }
    return EXIT_SUCCESS;
}
