#include"socket.h"
#include<netdb.h>
#include<string.h>
#include<errno.h>
#include<stdbool.h>
#include<stdio.h>
#include<stdlib.h>

#include<arpa/inet.h>
#include<netinet/in.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h> 

int socket_resolve(const char *host, const char *port, struct addrinfo **hostinfo){
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	return getaddrinfo(host, port, &hints, hostinfo);
}

int socket_dial(const char *host, const char *port){
	struct addrinfo *hostinfo;
	int socket_fd = -1;
	if(socket_resolve(host, port, &hostinfo) < 0){
		return -1;
	}

	for(struct addrinfo *p = hostinfo; p != NULL; p = p->ai_next){
		if((socket_fd = socket(p->ai_family, p->ai_socktype,p->ai_protocol))<0){
			continue;
		}
		if (connect(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
		    printf( "Unable to connect\n");
		    close(socket_fd);
	  	    continue;
		}
	}
	freeaddrinfo(hostinfo);
	return socket_fd;
}

int socket_listen(const char *port){
	struct addrinfo **clientinfo;
	int server_fd = -1;
	memset(*clientinfo,0,sizeof(*clientinfo));
	if(socket_resolve(NULL, port, clientinfo) < 0){
		fprintf(stderr,"Something fucked up. %s\n",strerror(errno));
		return EXIT_FAILURE;
	}

	for(struct addrinfo *p = *clientinfo; p != NULL; p = p->ai_next){
		if((server_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol))<0){
			continue;
		}
	}

	return 0;
}
