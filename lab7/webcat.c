#include "socket.h"
#include<stdio.h>
#include<stdlib.h>

void print_usage(){
	printf("Usage: ./webcat [url]");
	return;
}

int main(int argc, char **argv){
	char *url;
	if(argc >= 1){
		url = argv[1];
		printf("%s\n",url);
	} else {
		print_usage();	
	}
	return EXIT_SUCCESS;
}
