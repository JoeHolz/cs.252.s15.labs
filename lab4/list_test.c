#include "list.h"
#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

static const long   TEST_DATA[] = {5, 4, 7, 0, 1};
static const size_t TEST_SIZE   = sizeof(TEST_DATA)/sizeof(long);

void test_node_create(){
	printf("Testing node create\n");
    struct node_t *node;
    node = node_create((void*)INT_MAX, NULL, NULL);
    assert(node != NULL);
    assert(node->data == (void*)INT_MAX);
    assert(node->next == NULL);
    assert(node->prev == NULL);
    node_delete(node, false);
}

void test_list_create(){
	printf("Testing list create\n");
    struct list_t *list;
    list = list_create();
    assert(list != NULL);
    assert(list->size == 0);
    assert(list->head == NULL);
    assert(list->tail == NULL);
    list_delete(list, false);
}

void test_list_size(){
	printf("Testing list size\n");
    struct list_t *list;
    list = list_create();
    assert(list != NULL);
    for (size_t i = 0; i < TEST_SIZE; i++) {
        list_push_front(list, (void *)TEST_DATA[i]);
    }
    assert(list_size(list) == TEST_SIZE);
    list_delete(list, false);
}

void test_list_remove(){
	printf("Testing list remove\n");
	struct list_t *list = list_create();
	assert(list != NULL);
	int data = 5;
	list_push_front(list, &data);
	list_remove(list, list->head);
	assert(list->size == 0);
	list_delete(list,true);
}
void test_list_find(){
	printf("Testing list find\n");
	struct list_t *list;
	list = list_create();
	assert(list != NULL);
	for(size_t i =0; i< TEST_SIZE; i++){
		list_push_front(list,(void *)TEST_DATA[i]);
	}
	assert(list_size(list) == TEST_SIZE);
	struct node_t *found = list_find(list, (void*)4);
	long value = (long)found->data;
	assert(value == 4);
	list_delete(list, false);
}
void test_list_front(){
	printf("Testing list push and pop front\n");
	struct list_t *list = list_create();
	assert(list != NULL);
	for(int i = 0; i < TEST_SIZE; i++){
		list_push_front(list,(void *)TEST_DATA[i]);
	}
	for(int i = 0; i < TEST_SIZE; i++){
		void *data = list_pop_front(list);
//		assert((long int) data == TEST_DATA[i]);
	}
	list_delete(list,false);
}
void test_list_back(){
	printf("Testing list push and pop back\n");
	struct list_t *list = list_create();
	assert(list != NULL);
	for(int i =0; i < TEST_SIZE; i++){
		list_push_back(list,(void *)TEST_DATA[i]);
	}
	for(int i =0; i < TEST_SIZE; i++){
		void *data = list_pop_back(list);
//		assert((long int) data == TEST_DATA[i]);
	}
	list_delete(list,false);
}
int main(int argc, char *argv[]){
    test_node_create();
    test_list_create();
    test_list_size();
    test_list_remove();
    test_list_find();
    test_list_front();
    test_list_back();
    return EXIT_SUCCESS;
}
