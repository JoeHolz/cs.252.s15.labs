#include <stdlib.h>
#include <stdbool.h>
#ifndef __LIST_H__
#include "linkedlist.h"
#endif
struct node_t *create_node(void *data, struct node_t *next, struct node_t *prev){
	malloc(sizeof(*data));
	struct node_t *node;
	node->next = next;
	node->prev = prev;
	node->data = data;
	return node;
}
void node_delete(struct node_t *node, bool free_data){
	if(node == NULL){
		return;
	}
	//Delete the Node:
	if(free_data){
		free(node->data);
	}
	node->prev->next = node->next;
	node->next->prev = node->prev;
	return;
}
struct list_t *list_create(){
	struct list_t *list;
	list->head = create_node(NULL,list->tail,NULL);
	list->tail = create_node(NULL,NULL,list->head);
	return list;
}
void list_delete(struct list_t *list, bool free_data){
	if(list == NULL){
		return;
	}
	while(list->size){		
		node_delete(list->head,free_data);
	}
	if(free_data){
		free(list);
	}
	return;
}
size_t list_size(struct list_t *list){
	if(list == NULL){
		return -1;
	}else{
		return list->size;
	}
}
void list_push_front(struct list_t *list, void *data){
	struct node_t *newnode = create_node(data,list->head->next,list->head);
	list->head->next = newnode;
	list->head->next->prev = newnode;
	return;
}
void list_push_back(struct list_t *list, void *data){
	struct node_t *newnode = create_node(data,list->tail->next,list->tail);
	list->tail->next = newnode;
	return;
}
void *list_pop_back(struct list_t *list){
	return NULL;
}
struct node_t *list_find(struct list_t *list, void *data){
	return NULL;
}
void list_remove(struct list_t *list, struct node_t *node){
	node->prev->next = node->next;
	node->next->prev = node->prev;
	node_delete(node,true);	
	return;
}
