#include <stdlib.h>
#include <stdbool.h>
#include "list.h"
struct node_t *node_create(void *data, struct node_t *next, struct node_t *prev){
	struct node_t *node = malloc(sizeof(struct node_t));
	node->next = next;
	node->prev = prev;
	node->data = data;
	return node;
}
void node_delete(struct node_t *node, bool free_data){
	if(node == NULL){
		return;
	}
	//Delete the Node:
	if(free_data){
		free(node->data);
	}
	free(node);
	return;
}
struct list_t *list_create(){
	struct list_t *list = malloc(sizeof(struct list_t));
	list->size = 0;
	list->head = NULL;
	list->tail = NULL;
	return list;
}
void list_delete(struct list_t *list, bool free_data){
	if(list == NULL){
		return;
	}
	if(list->head != NULL){
		struct node_t *node=list->head;
		struct node_t *temp = NULL;
		while(node->next != NULL){	
			temp = node->next;
			node_delete(node,free_data);
			node = temp;
		}
	}
	free(list->tail);
	free(list);
	return;
}
int list_size(struct list_t *list){
	if(list == NULL){
		return -1;
	}else{
		return list->size;
	}
}
void list_push_front(struct list_t *list, void *data){
	struct node_t *newnode = node_create(data,NULL,NULL);
	if(list->head!=NULL){
		newnode->next=list->head;
		list->head->prev = newnode;
		list->head=newnode;
		list->size++;
	} else {
		list->head=list->tail=newnode;
		list->size++;
	}
	return;
}
void *list_pop_front(struct list_t *list){
	if(list->size > 1){
		list->size--;
		struct node_t *node = list->head->next;
		void *temp = list->head->data;
		free(list->head);
		list->head = node;
		return temp;
	} else if(list->size == 1){
		list->size--;
		void *temp = list->head->data;
		free(list->head);
		list->head = NULL;
		list->tail = NULL;
		return temp;
	} else {
		return NULL;
	}
}
void list_push_back(struct list_t *list, void *data){
	struct node_t *newnode = node_create(data,NULL,NULL);
	if(list->tail != NULL){
		newnode->prev=list->tail;
		list->tail->next = newnode;
		list->tail = newnode;
		list->size++;
	} else {
		list->head=list->tail=newnode;
		list->size++;
	}
	return;
}
void *list_pop_back(struct list_t *list){
	if(list->size > 1){
		list->size--;
		struct node_t *node = list->tail->prev;
		void *temp = list->tail->data;
		free(list->tail);
		list->tail = node;
		return temp;
	} else if(list->size == 1){
		list_pop_front(list);	
	} else {
		return NULL;		
	}
}
struct node_t *list_find(struct list_t *list, void *data){
	if(data == NULL){
		return NULL;
	}
	struct node_t *curNode = list->head;
	while(curNode != NULL && curNode->data != data){
			curNode = curNode->next;	
	}
	return curNode;
}
void list_remove(struct list_t *list, struct node_t *node){
	if(node == NULL || list->size == 0){
		return;
	} else if(list->size > 1){
		node->prev->next = node->next;
		node->next->prev = node->prev;
		node_delete(node,true);
		list->size--;	
	} else if(list->size == 1){
		free(list->head);
		list->head = NULL;
		list->tail = NULL;
		list->size = 0;
	}
}
