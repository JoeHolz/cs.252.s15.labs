#ifndef __LIST_H__
#define __LIST_H__
#include <stdbool.h>
struct node_t{
	void 		*data;
	struct node_t   *next;
	struct node_t   *prev;
};
struct list_t{
	size_t size;
	struct node_t *head;
	struct node_t *tail;
};
struct node_t *create_node(void *data, struct node_t *next, struct node_t *prev);
void node_delete(struct node_t *node, bool free_data);
struct list_t *list_create();
void list_delete(struct list_t *list, bool free_data);
size_t list_size(struct list_t *list);
void list_push_front(struct list_t *list, void *data);
void list_push_back(struct list_t *list, void *data);
void *list_pop_back(struct list_t *list);
struct node_t *list_find(struct list_t *list, void *data);
void list_remove(struct list_t *list, struct node_t *node);
#endif
