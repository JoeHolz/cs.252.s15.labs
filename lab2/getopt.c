#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

void
print_usage(const char *program)
{
    fprintf(stderr, "usage: %s [nhv] arguments...\n", program);
}

int
main(int argc, char *argv[])
{
    char *optarg;
    int optind;
    int  c;
    int  number  = -1;
    bool verbose = false;

    while ((c = getopt(argc, argv, "n:h?v")) != -1) {
    	switch (c) {
	    case 'n':
	    	number = atoi(optarg);
	    	break;
	    case 'v':
	    	verbose = true;
	    	break;
	    case '?':
	    case 'h':
	    	print_usage(argv[0]);
	    	return EXIT_SUCCESS;
	    default:
	    	print_usage(argv[0]);
	    	return EXIT_FAILURE;
	}
    }

    printf("Number:  %d\n", number);
    printf("Verbose: %s\n", verbose ? "True" : "False");

    for (int i = optind; i < argc; i++) {
    	printf("Positional argument : %s\n", argv[i]);
    }

    return EXIT_SUCCESS;
}
