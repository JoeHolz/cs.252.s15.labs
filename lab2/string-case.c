#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

void printlower(char buffer[BUFSIZ]){
	fgets(buffer, BUFSIZ, stdin);
        for(int i = 0; i < BUFSIZ;i++){
        	buffer[i] = tolower(buffer[i]);
        }
	fputs(buffer, stdout);
	return;
}

void printupper(char buffer[BUFSIZ]){
	fgets(buffer,BUFSIZ,stdin);
	for(int i = 0; i < BUFSIZ; i++){
		buffer[i] = toupper(buffer[i]);
	}
	fputs(buffer, stdout);
	return;
}

int main(int argc, char *argv[]){
	int option = 0;
	char buffer[BUFSIZ];

	while((option = getopt(argc, argv,"luh?")) != -1){
		switch (option){
			case 'l':
				printlower(buffer);
				break;
			case 'u':
				printupper(buffer);
				break;

			case '?':
			case 'h':
				printf("usage: ./string-case [-luh]");
				break;

			default:
				return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}
