#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void count_characters(){
	int numchars = 0;
	printf("Characters: %d\n",numchars);
}
void count_lines(){
	int numlines = 0;
	printf("Lines: %d\n",numlines);
}
void count_words(){
	int numwords = 0;
	printf("Words: %d\n",numwords);
}

int main(int argc, char *argv[]){
    int optarg =0;
    int c = 0;
    while ((c = getopt(argc, argv, "cwlh")) != -1) {
    	switch (c) {
	    case 'c':
		count_characters();
	    	break;
	    case 'l':
		count_lines();
	    	break;
	    case 'w':
		count_words();
	    case '?':
	    case 'h':
		printf("usage: ./string-count [-cwlh]\n-c count characters\n-w count words\n-l count lines\n");
	    	return EXIT_SUCCESS;
	    default:
	    	return EXIT_FAILURE;
	}
    }
    return EXIT_SUCCESS;
}
