//usage: ./string-search [invh] needle
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>

void print_result(int line, char *printme, bool printline){
	if(printline){
		printf("%d: %s",line,printme);
	}else{
		printf("%s", printme);
	}
}

int main(int argc, char *argv[]){
	int option = 0;
        char buffer[BUFSIZ];
 	bool ignorecase = false;
	bool linenumbers = false;
	bool invertsearch = false;

        while((option = getopt(argc, argv,"invh")) != -1){
                switch (option){
                        case 'i':
                                ignorecase = true;
				break;
           		case 'n':
                                linenumbers = true;
				break;
			case 'v':
				invertsearch = true;
				break;
			case 'h':
			case '?':
                                printf("usage: ./string-search [-invh] needle\n -i Ignore Case\n-v Invert Match\n-h Halp!");
				break;
			default:
				return EXIT_FAILURE;
		}
	}
	char *needle = argv[argc-1]; 
	int i = 0;
	while(fgets(buffer, BUFSIZ,stdin) != NULL){
		if(ignorecase){
			if(strcasestr(buffer,needle)){
				if(!invertsearch){
					print_result(i+1,buffer,linenumbers);
					}
			}else{
				if(invertsearch){
					print_result(i+1,buffer,linenumbers);
				}	
		}}else{
			if(strstr(buffer,needle)){
                         if(!invertsearch){
                                 print_result(i+1,buffer,linenumbers);
                         }
                 }else{
                         if(invertsearch){
                                 print_result(i+1,buffer,linenumbers);
                         }
                 }}
		i++;
	}
}
