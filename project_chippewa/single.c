/* single.c: Single User HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

/**
 * Single connection version of HTTP server that processes one request at a
 * time.
 **/
void
single_server(int sfd)
{
    struct request *request;

    /* Accept and handle HTTP request */
    while (1) {
    	/* Accept request */
        request = accept_request(sfd);
	/* Handle request */
    	
        handle_request(request);
	/* Free request */
    	free_request(request);
    }

    /* Close server socket and exit */
    free(sfd);
    exit(0);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
