/* chippewa: Simple HTTP Server */
#include "chippewa.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <arpa/inet.h>
/* Global Variables */
char *Port		    = "41681";
char *MimeTypesPath	    = "/etc/mime.types";
char *DefaultMimeType	    = "text/plain";
char *RootPath		    = ".";
mode  ConcurrencyMode	    = SINGLE;
pthread_mutex_t PopenMutex  = PTHREAD_MUTEX_INITIALIZER;
char* path;

/**
 * Display usage message.
 */
static void
usage(const char *progname)
{
    fprintf(stderr, "usage: %s [hcmMpr]\n", progname);
    fprintf(stderr, "options:\n");
    fprintf(stderr, "    -h            Display help message\n");
    fprintf(stderr, "    -c mode       Single, Forking, or Threaded mode\n");
    fprintf(stderr, "    -m path       Path to mimetypes file\n");
    fprintf(stderr, "    -M mimetype   Default mimetype\n");
    fprintf(stderr, "    -p port       Port to listen on\n");
    fprintf(stderr, "    -r path       Root directory\n");
}

/**
 * Parses command line options and starts appropriate server
 */
int
main(int argc, char *argv[])
{   
    int server_fd = -1;
    int curArg = 1;
    
    /* Parse command line options */
    while(argc - curArg){
        if(strstr(argv[curArg],"-c")){
                //Sets Concurrency Mode
                curArg++;
            if(strstr(argv[curArg],"single")){
                ConcurrencyMode = SINGLE;       
                //printf("setting mode to single\n");         
            } if (strstr(argv[curArg],"forking")){
                 ConcurrencyMode = FORKING;
                 //printf("setting mode to forking\n");
            } if(strstr(argv[curArg],"threaded")){
                ConcurrencyMode = THREADED;
                //printf("setting mode to threaded\n");
            }                
        }    
        if(strstr(argv[curArg],"-h")){
            //Print Usage
            usage(argv[0]);
            return EXIT_SUCCESS;
        }
        if(strstr(argv[curArg],"-m")){
            //Set MimeTypes Path
            MimeTypesPath = argv[++curArg];
        }
        if(strstr(argv[curArg],"-M")){
            //Set Default Mimetype
            DefaultMimeType = argv[++curArg];
        }
        if(strstr(argv[curArg],"-p")){
            //Port
            Port = argv[++curArg];
        }
        if(strstr(argv[curArg],"-r")){
            //Path to root
            RootPath = argv[++curArg];
        }
        curArg++;
    }
    char buff[BUFSIZ];
    server_fd = socket_listen(Port);
    realpath(RootPath, buff);
    RootPath = buff; 

    log("Listening on port %s", Port);
    debug("RootPath        = %s", RootPath);
    debug("MimeTypesPath   = %s", MimeTypesPath);
    debug("DefaultMimeType = %s", DefaultMimeType);
    debug("ConcurrencyMode = %s", ConcurrencyMode == 0 ? "Single" : ConcurrencyMode == 1 ? "Forking" : "Threaded");
    
    








        
    }
    return EXIT_SUCCESS;
}
/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
