/* socket.c: Simple Socket Functions */
#include "chippewa.h"
#include <stdbool.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>

/**
 * Allocate socket, bind it, and listen to specified port.
 **/
int
socket_listen(const char *port)
{
    	struct addrinfo  hints;
    	struct addrinfo *results;
    	int    socket_fd = -1;
    	int status;
//V This code lovingly stolen from Beej's Guide to Network Programming V
    	memset(&hints, 0, sizeof hints); // make sure the struct is empty
    	hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
    	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    	hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

        if ((status = getaddrinfo(NULL, port, &hints, &results)) != 0) {
        	fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        	return EXIT_FAILURE;
    	}
// servinfo now points to a linked list of 1 or more struct addrinfos
//
// // ... do everything until you don't need servinfo anymore ....
//^This code lovingly stolen from Beej's Guide to Network Programming^

    /* Lookup server address information */
    /* TODO */
    /* For each server entry, allocate socket and try to connect */
	for(struct addrinfo *p = results; p != NULL; p=p->ai_next){
	/* Allocate socket */
		char ip[INET6_ADDRSTRLEN];
		if(p->ai_family == AF_INET){
			inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr),ip,sizeof(ip));
		}else{
			inet_ntop(p->ai_family, &(((struct sockaddr_in6 *)p->ai_addr)->sin6_addr),ip,sizeof(ip));
		}
		if((socket_fd = socket(p->ai_family,p->ai_socktype,p->ai_protocol)) < 0){
			fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
			continue;
		}

	/* Bind socket */
		if(bind(socket_fd, p->ai_addr,p->ai_addrlen)<0){
			fprintf(stderr,"unable to bind to %s:%s %s\n",ip,port,strerror(errno));
			close(socket_fd);
			continue;
		}

    	/* Listen to socket */
        //	while(true){
			if(listen(socket_fd, SOMAXCONN) < 0){
				fprintf(stderr,"Unable to listen on %s:%s %s\n",ip,(char *)port, strerror(errno));
				close(socket_fd);
				continue;
			}
	//	}
		fprintf(stderr,"Listening on %s:%s\n", ip, port);
		break;
	}
	freeaddrinfo(results);
	return socket_fd;
}
/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
