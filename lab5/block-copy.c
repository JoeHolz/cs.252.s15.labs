#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void print_usage(){
	printf("Usage: if=\'inputfile\'\nof=\'outputfile\'\n");
}
void copy(FILE *infile, FILE *outfile,int count,int seek,int skip, int bytes){
	printf("copying %d bytes from %0x to %0x, skipping %d bytes of input and %d bytes of output\n",bytes,infile,outfile,seek,skip);
//	printf("count: %d\n",count);
	char buffer;
	int copied = 1;
	if(copied <= skip){
		while((buffer = getc(infile) != EOF)&&(copied < skip)){
			copied++;
		}
	}
	while(((buffer = getc(infile)) != EOF) && ((copied <= bytes) || bytes == 0)){
		if(copied >= seek){
			putc(buffer,outfile);
		}
		copied++;
	}
	printf("done!\n");
}
int main(int argc, char*argv[]){
	//Vars for handling command line options.	
	FILE *infile = NULL;
	FILE *outfile = NULL;
	char *in = NULL;
	char *out = NULL;
	int count = 0;
	int seek = 0;
	int skip = 0;
	int bytes = 0;
	argc--;	
	while(argc){
		if(strstr(argv[argc],"if")){
			in = argv[argc]+3;
			infile = fopen(argv[argc]+3,"r");
			if(infile == NULL){
				printf("could not open %s\n",in);
			}
		}
		if(strstr(argv[argc],"of")){
			out = argv[argc]+3;
			outfile = fopen(argv[argc]+3,"w");
			if(outfile == NULL){
				printf("could not open %s\n",out);
			}
		}
		if(strstr(argv[argc],"count=")){
			count=atoi(argv[argc]+6);
		}
		if(strstr(argv[argc],"seek=")){
			seek=atoi(argv[argc]+5);
		}
		if(strstr(argv[argc],"skip=")){
			skip=atoi(argv[argc]+5);
		}
		if(strstr(argv[argc],"bs=")){
			bytes=atoi(argv[argc]+3);
		}
		argc--;
	}
	//printf("infile: %s\n outfile: %s\n count: %d \n seek: %d\n skip: %d\n", in, out, count, seek, skip);
	copy(infile,outfile,count,seek,skip,bytes);
	return EXIT_SUCCESS;		
}
