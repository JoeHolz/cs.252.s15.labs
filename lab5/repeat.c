//Partial reimplementation of tee
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>

void print_usage(){
	printf("usage: ./repeat -[ah] FILE ...\n");
	printf("\t-a\tAppend to given FILEs (as opposed to overwrite)\n");
	printf("\t-h\tDisplay this help and exit\n");
}
void repeat(FILE *fp[], int numfiles){
	char buffer[BUFSIZ];
	while ((fread(buffer, 1, BUFSIZ,stdin)) > 0) {
			if(numfiles){
				for(int i = 0; i < numfiles; i++){
					printf("writing buffer to file %d\n",i);
					fputs(buffer,fp[i]);
				}
			} else {
				printf(buffer);
			}
		}
}
int main(int argc, char **argv){
	int i = 0;
	char *mode = "w";
	//Get Args
	if(strcmp(argv[i],"-a") == 0){
		mode = "a";
		i++;
	}else if(strcmp(argv[i],"-h") == 0){
		print_usage();
		return EXIT_SUCCESS;
	}
	//Get File Args:
	if(argc > 1){//This means "if we have file arguments"
		FILE *fp[argc];
		memset(fp,0,argc);
		//Open the files.
		for(int j = 0; j < argc - 1; j++){
			printf("%d\n",j);
			if((fp[j] = fopen(argv[j+1],mode)) != NULL){
				printf("%s opened for output. Placed in slot %d of fp[]\n",argv[j+1],j);
			} else {
				printf("error during open file: %s\n", strerror(errno));
			}
		}
		printf("numfiles = %d\n",argc-1);
		repeat(fp,argc-1);
	} else {
		printf("no file arguments, printing to stdout\n");
		FILE *fp[1];
		repeat(fp,0);
	}
//	for(int j = i; j < argc -i +1; j++){
//		printf("%s",argv[j]);
//	}
	return EXIT_SUCCESS;
}
