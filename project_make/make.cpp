//#include<cstdio>
//#include<string>
//#include<cstdlib>
#include<cstdbool>
#include "graph.h"
#include<iostream>
#include<fstream>
#include<string>

using namespace std;

void printUsage(){
	//Tells the User how to use it.
	cout << ("Usage: ./make [-f <makefile>] [target]\nUses \'makefile\' as a makefile by default.\n");
}

void echoTarget(char *target){
	printf("%s\n",target);
}

//process comments
string processword(string name){
	while(name.find("#") != string::npos){
		size_t beginning = name.find("#");
		name = name.erase(beginning, (name.find("#", beginning) - beginning) + 2);
	}
	return name;
}

//remove whitespace
string whitespace(string name){
	string whitespace = " \t";
	const auto sBegin = name.find_first_not_of(whitespace);
	if(sBegin == string::npos){
		return "";
	}
	const auto sEnd = name.find_last_not_of(whitespace);
	const auto sRange = sEnd - sBegin + 1;
	return name.substr(sBegin, sRange);
}


void parse_variable(Graph &g, ifstream fp, string line){
	string name;
	string value;
	size_t split;
	if((split = line.find("=")) != string::npos){
		name = line.substr(0, split);
		value = line.substr(split+1, string::npos);
	}
	name = processword(name);
	value = processword(value);
	name = whitespace(name);
	value = whitespace(value);
	const char *eName = name.c_str();
	const char *eValue = value.c_str();
	setenv(eName,eValue, true);

}

void parse_rule(Graph &g, ifstream fp, string line){
	string output;
	string inputs;
	size_t split;
	if((split = line.find(":")) != string::npos){
		output = line.substr(0, split);
		inputs = line.substr(split+1, string::npos);
	}
/*
	string find = line.find(":");
	output = strtok(line, find);
	inputs = strtok(NULL, find);
*/
	string goodout;
	goodout = whitespace(output);

	string nocommentinps;
	nocommentinps = processword(inputs);
	string []fininputs = inputsarray(nocommentinps);
	int i;
	int length;
	for(i = 0; i < length; i++){
		string trim = whitespace(fininputs[i]);
		fininputs[i] = trim;
	}
	string command;
	getline(fp, command);
	command = processword(command);
	command = whitespace(command);
	Node current; // create node based off all this info
	time_t timer;
	current.target = goodout;
	current.sources = fininputs;
	current.command = command;
	current.timestamp = time(&timer);

	//now we gonna create a node, and add it to das graph.
}

void parse_stream(Graph &g, string makefilepath){
	string line = "";
	string temp;
	ifstream in;
	in.open(makefilepath);
		while(!in.eof()){
			getline(in, line);
			cout << line << endl;
			if(line.find("=", 0) != npos){
				parse_variable(g, in, line);
			}else if(line.find(":", 0) != npos){
				parse_rule(g, in, line);
			}
		}
	in.close();
	return 0;

}

//split inputs into array
string []inputsarray(string input){
	string ptr;
	string returnvar[];
	ptr = strtok(input, " ");
	size_t i = 0;
	while(ptr != NULL){
		returnvar[i] = ptr;
		i++;
		ptr = strtok(NULL, " ");
	}
	return returnvar;
}

int main (int argc, char *argv[]){
	bool targetset = false;
	bool debug = false;
	int curArg = 1;
	char *makefilepath = "makefile";
	char *target = "all";
	if(debug){printf("argc = %d\n",argc);}
		while(curArg < argc){
			if(debug){printf("Parsing: %s\n",argv[curArg]);}
		//If the current argument contains -f, set makfile = the next arg.
		if(strstr(argv[curArg],"-f")){
			curArg++;
			if(argc > curArg){//Dodges the segfault possible if User gives -f with no argument.
				makefilepath = argv[curArg];
			} else {
				printf("You dick. If you're gonna use -f, PLEASE follow it with a filename.\n");
			}
		} if(strstr(argv[curArg],"-h")){
			printUsage();
			return EXIT_SUCCESS;
		} if(!targetset){
				target = argv[curArg];
				targetset = true;
			if(debug){printf("target is: %s\n",target);}
		}
		curArg++;
	}

//	echoFile(makefilepath);
//	echoTarget(target);
	if(debug){printf("Using makefile %s and target %s.\n",makefilepath,target);}
		return EXIT_SUCCESS;
	}

