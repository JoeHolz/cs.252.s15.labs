#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>
#include "graph.h"
#include<iostrem>
#include<fstream>

void printUsage(){
	//Tells the User how to use it.
	cout << ("Usage: ./make [-f <makefile>] [target]\nUses \'makefile\' as a makefile by default.\n");
}

FILE *openFile(char *makefilepath){
//	opens and prints makefile. Will later be modified to open and parse rules, and vars out of makefile.
//	Probably will then be called "parseMakeFile()"
	FILE *makefile;
	if((makefile = fopen(makefilepath, "r"))){
		return makefile;
	} else {
		printf("Couldn't find a makefile by name \'%s\'. Please make one.\n",makefilepath);
		exit(0);
	}
}

void echoTarget(char *target){
	printf("%s\n",target);
}

void parse_stream(Graph &h, string makefilepath){
	string line;
	ifstream in (makefilepath);
	if(in.is_open()){
		while(!in.eof()){
			getline(in, line);
			string m = line.find("=");
			string n = line.find(":");
			if(line.find(m) != npos){
				parse_variable(g, in, line);
			}else if(line.find(n) != npos){
				parse_rule(g, in, line);
			}
		}
	in.close();
	}else{
		cout << "unable to open file";
	}
	return 0;

/*	File *fp;
	fp = openFile(makefilepath);
	char line[256];
	while(fgets(line, sizeof(line), fp)){
		if(strstr(line, "=")){
			parse_variable(g, fp, line);
		}else if(strstr(line, ":")){
			parse_rule(g, fp, line);
		}else {
			//check for empty line(skip), check for comment, else syntax error?
		}
	}
*/ /*
	for line in fs:
		if '=' in line:
			parse_variable(dag, fs, line)
		elif ':' in line:
			parse_rule(dag, fs, line)
	*/
}

void parse_variable(Graph &g, ifstream fp, string line){
	string name;
	string value;
	string find = line.find("=");
	name = strtok(line, find);
	value = strtok(NULL, find);
	string nametrim;
	string valtrim;
	nametrim = processword(name);
	valtrim = processword(value);
	nametrim = whitespace(nametrim);
	valtrim = whitespace(valtrim);
	if(nametrim != NULL && valtrim != NULL){
		setenv(nametrim, valtrim, 1);
	}
/*	char *name;
	char *value;
	char *find = "=";
	name = strtok(line, find);
	value = strtok(NULL, find);
	printf("Give me a name: %s\n", name);
	printf("Give me a value: %s\n", value);
	char *nametrim;
	char *valtrim;
	nametrim = processword(name);
	valtrim = processword(value);
	if(nametrim != NULL && valtrim != NULL){
		setenv(nametrim, valtrim, 1);
	}*/
}

void parse_rule(Graph &g, ifstream fp, string line){
	string ouspus;
	string inputs;
	string find = line.fine(":");
	output = strtok(line, find);
	inputs = strtok(NULL, find);
	string goodout;
	goodout = whitespace(output);

	string nocommentinps;
	nocommentinps = processword(inputs);
	string []fininputs = inputsarray(nocommentinps);
	int i;
		int length;
	for(i = 0; i < length; i++){
		string trim = whitespace(fininputs[i]);
		fininputs[i] = trim;
	}
	string command;
	getline(fp, command);
	command = processword(command);
	command = whitespace(command);
/*	char *output;
	char *inputs;
	char *find = ":";
	output = strtok(line, find);
	inputs = strtok(NULL, find);
	char *goodout;
	//remove white space of output
	goodout = whitespace(output);
	//goodout is output with no white space.. now onto removing comments from inputs.
	char **nocommentinps;
	nocommentinps = processword(inputs);
	char *fininputs[] = inputsarray(nocommentinps);
	//now loop through this array, grab each item and delete all the whitespace from it.
	int i;
	int length = strlen(finiputs);
	for( i = 0; i < length;i++){
		whitespace(fininput[i]);
		//put newly trimmed inputs back into fininputs array..
		fininput[i] = trim;
	}
	//read next line, clean up and store into command as the rule
	char *command;
	fgets(command, sizeof(command), fp);
	//remove comments, remove whitespace from command which holds the next line read in hopefully.
	processword(command);
	whitespace(command);
*/
	//now we gonna create a node, and add it to das graph. 
}

//split inputs into array
string []inputsarray(string input){
	string ptr;
	string returnvar[];
	ptr = strtok(input, " ");
	size_t i = 0;
	while(ptr != NULL){
		returnvar[i] = ptr;
		i++;
		ptr = strtok(NULL, " ");
	}
	return returnvar;
}

//process comments
string processword(string name){
	string nametrim;
	nametrim = strchr(name,'#'); // remove comments
	if(nametrim){
		*nametrim = '\0';
	}
	return nametrim;
}

//remove whitespace
string whitespace(string name){
	string nametrim = name + strlen(name) - 1; //delete ending trim
	while(isspace(nametrim)){
		s--;
	}
	(nametrim + 1) = "\0"; // add ending char
	nametrim = name;
	while(isspace(nametrim)){ // remove leading trim
		nametrim++;
	}
	return nametrim;
}

int main (int argc, char *argv[]){
	bool targetset = false;
	bool debug = false;
	int curArg = 1;
	char *makefilepath = "makefile";
	char *target = "all";
	if(debug){printf("argc = %d\n",argc);}
		while(curArg < argc){
			if(debug){printf("Parsing: %s\n",argv[curArg]);}
		//If the current argument contains -f, set makfile = the next arg.
		if(strstr(argv[curArg],"-f")){
			curArg++;
			if(argc > curArg){//Dodges the segfault possible if User gives -f with no argument.
				makefilepath = argv[curArg];
			} else {
				printf("You dick. If you're gonna use -f, PLEASE follow it with a filename.\n");
			}
		} if(strstr(argv[curArg],"-h")){
			printUsage();
			return EXIT_SUCCESS;
		} if(!targetset){
				target = argv[curArg];
				targetset = true;
			if(debug){printf("target is: %s\n",target);}
		}
		curArg++;
	}

//	echoFile(makefilepath);
//	echoTarget(target);
	if(debug){printf("Using makefile %s and target %s.\n",makefilepath,target);}
		return EXIT_SUCCESS;
	}

