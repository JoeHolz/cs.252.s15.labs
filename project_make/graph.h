#ifndef __GRAPH_H__
	#define __GRAPH_H__
#include<iostream>
#include<set>
#include<map>
#include<sstream>
#include<string>
#include"make.h"
/*
typedef std::set<Node> Edges;
typedef std:: map<Node, Edges> Graph;
*/
typedef std::map<std::string, Node> Graph;
/*
Idea.. How about getting rid of the whole edges thing.. not too sure why 
we're using that exactly.. The way I'm reading the dag section writeup, it 
seems that graph should consist of a map<string, Node> where string
is a name associated to the node. Then in the node class(make.h) it has 
the target(output), sources(inputs), command(command) and timestamp.. 
Not too sure but I think having all that in the node takes care of the whole
adjacency part because the inputs of the node track all that nodes 
dependencies (AKA things adjacent too it right??)
*/

Graph graph();

void print_graph(Graph &g);
void node_add(Graph &g, Node node);
void remove_node(Graph &g, std::string node);

#endif
