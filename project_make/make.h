#include <string>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <set>

typedef struct Nodes {
	std::string target;
	std::set<std::string> sources;
	std::string command;
	time_t timestamp;
} Node;
